require('datatables.net');

$(document).ready(function () {
    $('#table_users').DataTable();

    $(".user_delete").on("click", function () {
        var url = Routing.generate('user_delete', {
            'id': $(this).data('id')
        });

        var tr = $(this).closest("tr");

        $.ajax({
            url: url,
            type: 'POST',
            success: function (data) {
                if (undefined !== data.response && true == data.response) {
                    $(tr).remove();
                } else {
                    alert("Error : Please, try later or contact the administrator.");
                }
            }
        });
    });
});