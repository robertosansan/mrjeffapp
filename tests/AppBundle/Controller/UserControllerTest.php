<?php

namespace Tests\AppBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Test\WebTestCase;

class UserControllerTest extends WebTestCase {

    public function testList() {
        $client = static::createClient();

        $crawler = $client->request('GET', '/user/list');

        $this->assertEquals(200, $client->getResponse()->getStatusCode());
        $this->assertContains('List Users', $crawler->filter('.container h1')->text());
    }
    
    public function testEditExists() {
        $client = static::createClient();

        $crawler = $client->request('GET', '/user/edit/1');

        $this->assertEquals(200, $client->getResponse()->getStatusCode());
        $this->assertContains('Edit User', $crawler->filter('.container h1')->text());
    }
    
    public function testEditNotExists() {
        $client = static::createClient();

        $crawler = $client->request('GET', '/user/edit/999999999999999999999999999999999999999');

        $this->assertEquals(200, $client->getResponse()->getStatusCode());
        $this->assertContains('Error', $crawler->filter('.container h1')->text());
    }
    
    public function testEditIdNotNumber() {
        $client = static::createClient();

        $crawler = $client->request('GET', '/user/edit/a');

        $this->assertEquals(404, $client->getResponse()->getStatusCode());
    }

}
