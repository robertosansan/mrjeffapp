<?php

namespace Tests\AppBundle\Service;

use AppBundle\Service\UserService;
use Symfony\Bundle\FrameworkBundle\Test\WebTestCase;

class UserServiceTest extends WebTestCase {

    public function testNotUsefulMock() {
        $mock = $this->createMock(UserService::class);
        
        $mock->method('getAllUsers')
                ->willReturn('foo');
        
        $this->assertEquals('foo', $mock->getAllUsers());
        
    }
    
    public function testNotUsefulMock2() {
        $mock = $this->getMockBuilder(UserService::class)
                ->disableOriginalConstructor()
                ->getMock();
        
        $mock->expects($this->any())
                ->method('getAllUsers')
                ->willReturn('test');
        
        echo $mock->getAllUsers();
        
        $this->expectOutputString('test');
    }

}
