<?php

namespace AppBundle\Controller;

use AppBundle\Entity\User;
use AppBundle\Form\UserType;
use AppBundle\Service\UserService;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;

class UserController extends Controller {

    /**
     * @Route("/user/list", name="user_list")
     */
    public function listAction(UserService $userService) {
        $errors = array();

        $users = $userService->getAllUsers();

        if (is_null($users)) {
            $errors[] = "Error : Please try later or contact the administrator";
        }

        if (count($errors) > 0) {
            return $this->render('common/error.html.twig', array('errors' => $errors));
        }

        return $this->render('user/index.html.twig', array('users' => $users));
    }

    /**
     * @Route("/user/edit/{id}", name="user_edit", requirements={"id"="\d+"})
     */
    public function editAction($id, Request $request, UserService $userService) {
        $errors = array();

        try {
            $user = $userService->getUserById($id);
        } catch (NotFoundHttpException $e) {
            // in other cases, we can do something different than catch \Exception
            $errors[] = "Error : " . $e->getMessage();
        } catch (\Exception $e) {
            $errors[] = "Error : " . $e->getMessage();
        }

        if (count($errors) > 0) {
            return $this->render('common/error.html.twig', array('errors' => $errors));
        }

        $form = $this->createForm(UserType::class, $user);
        $form->add('submit', SubmitType::class, [
            'label' => 'Edit User',
            'attr' => ['class' => 'btn btn-success pull-right'],
        ]);

        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $user = $form->getData();
            
            if ($userService->editUser($user)) {
                $this->addFlash('success', 'User edited successfully.');
//                return $this->redirectToRoute('user_edit', array('id' => $id));
            } else {
                $this->addFlash('error', 'Error : Please try later or contact the administrator.');
            }
        }

        return $this->render('user/edit.html.twig', array('form' => $form->createView()));
    }

    /**
     * @Route("/user/delete/{id}", options={"expose"=true}, name="user_delete", requirements={"id"="\d+"})
     */
    public function deleteAction($id, UserService $userService) {
        $deleted = $userService->deleteUser($id);

        $response = new Response(json_encode(array('response' => $deleted)));
        $response->headers->set('Content-Type', 'application/json');

        return $response;
    }

}
