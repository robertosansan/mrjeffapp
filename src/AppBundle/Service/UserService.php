<?php

// src/AppBundle/Service/UserService.php

namespace AppBundle\Service;

use AppBundle\Entity\User;
use Psr\Log\LoggerInterface;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;

class UserService {

    private $apiUrl;
    private $logger;

    public function __construct(LoggerInterface $logger) {
        $this->apiUrl = "http://jsonplaceholder.typicode.com/users";
        $this->logger = $logger;
    }

    /**
     * 
     * @return array
     */
    public function getAllUsers() {
        $response = \Unirest\Request::get($this->apiUrl);

        $users = array();
        if (isset($response->code) && isset($response->body)) {
            if (200 == $response->code) {
                foreach ($response->body as $u) {
                    $user = new User();

                    $user->setId($u->id);
                    $user->setName($u->name);
                    $user->setUsername($u->username);
                    $user->setEmail($u->email);
                    $user->setPhone($u->phone);
                    $user->setWebsite($u->website);

                    $users[] = $user;
                }
            } else {
                $this->logger->error("UserService getUsers code " . $response->code);
                $users = null;
            }
        } else {
            $this->logger->error("UserService getUsers");
            $users = null;
        }

        return $users;
    }

    /**
     * 
     * @param int $id
     * @return User
     */
    public function getUserById($id) {
        $response = \Unirest\Request::get($this->apiUrl, null, array('id' => $id));

        if (isset($response->code) && isset($response->body)) {
            if (200 == $response->code) {
                if (sizeof($response->body) > 0) {
                    if (sizeof($response->body) == 1) {
                        $u = $response->body[0];

                        $user = new User();

                        $user->setId($u->id);
                        $user->setName($u->name);
                        $user->setUsername($u->username);
                        $user->setEmail($u->email);
                        $user->setPhone($u->phone);
                        $user->setWebsite($u->website);
                    } else {
                        $this->logger->error("UserService getUser more than one result");
                        throw new NotFoundHttpException('More than one result');
                    }
                } else {
                    $this->logger->error("UserService getUser no results");
                    throw new NotFoundHttpException('User not found');
                }
            } else {
                $this->logger->error("UserService getUser code " . $response->code);
                throw new Exception('Something went wrong : code ' . $response->code);
            }
        } else {
            $this->logger->error("UserService getUser");
            throw new Exception('Something went wrong');
        }

        return $user;
    }

    /**
     * 
     * @param User $user
     * @return boolean
     */
    public function editUser(User $user) {
        $response = \Unirest\Request::put($this->apiUrl, null, array(
                    'id' => $user->getId(),
                    'name' => $user->getName(),
                    'username' => $user->getUsername(),
                    'email' => $user->getEmail(),
                    'phone' => $user->getPhone(),
                    'website' => $user->getWebsite()
        ));

        $edited = true;
        if ((!isset($response->code)) || 200 != $response->code) {
            // not working, put don't exist, always HTTP/1.1 404 Not Found
            // but we continue as it worked
//            $edited = false;
        }

        return $edited;
    }
    
    /**
     * 
     * @param int $id
     * @return boolean
     */
    public function deleteUser($id) {
        $response = \Unirest\Request::delete($this->apiUrl, null, array('id' => $id));
        
        $deleted = true;
        if ((!isset($response->code)) || 200 != $response->code) {
            // not working, delete don't exist, always HTTP/1.1 404 Not Found
            // but we continue as it worked
//            $deleted = false;
        }
        
        return $deleted;
    }

}
