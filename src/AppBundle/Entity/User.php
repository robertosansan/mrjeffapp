<?php

// src/AppBundle/Entity/User.php

namespace AppBundle\Entity;

use Symfony\Component\Validator\Constraints as Assert;

class User {

    private $id;
    
    /**
     * @Assert\NotBlank()
     */
    private $name;
    
    /**
     * @Assert\NotBlank()
     */
    private $username;
    
    /**
     * @Assert\NotBlank()
     */
    private $email;
    
    /**
     * @Assert\NotBlank()
     */
    private $phone;
    
    /**
     * @Assert\NotBlank()
     */
    private $website;

    /**
     * 
     * @param int $id
     */
    public function setId($id) {
        $this->id = $id;
    }

    /**
     * 
     * @return int
     */
    public function getId() {
        return $this->id;
    }

    /**
     * 
     * @param string $name
     */
    public function setName($name) {
        $this->name = $name;
    }

    /**
     * 
     * @return string
     */
    public function getName() {
        return $this->name;
    }

    /**
     * 
     * @param string $username
     */
    public function setUsername($username) {
        $this->username = $username;
    }

    /**
     * 
     * @return string
     */
    public function getUsername() {
        return $this->username;
    }

    /**
     * 
     * @param string $email
     */
    public function setEmail($email) {
        $this->email = $email;
    }

    /**
     * 
     * @return string
     */
    public function getEmail() {
        return $this->email;
    }

    /**
     * 
     * @param string $phone
     */
    public function setPhone($phone) {
        $this->phone = $phone;
    }

    /**
     * 
     * @return string
     */
    public function getPhone() {
        return $this->phone;
    }

    /**
     * 
     * @param string $website
     */
    public function setWebsite($website) {
        $this->website = $website;
    }

    /**
     * 
     * @return string
     */
    public function getWebsite() {
        return $this->website;
    }

}
